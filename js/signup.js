const alphabet = ["_", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];
function SliderSetValue(val) {
	document.getElementById('output' + val).innerHTML = alphabet[document.getElementById('slider' + val).value];
	PlayAudio("../media/wormdelicious-ES.mp3");
}
function AddRanValue(max) {
	document.getElementById('password').innerHTML = document.getElementById('password').innerHTML + alphabet[Math.floor(Math.random() * max)];
}
function ClearInputs(errorMsg) {
	for(i = 1; i <= 12; i++) {
		document.getElementById('output' + i).innerHTML = "_";
		document.getElementById('slider' + i).value = 0;
	}
	document.getElementById('password').innerHTML = "";
	document.getElementById('pronouns').value = "";
	document.getElementById('friendCode').value = "";
	window.alert(errorMsg);
}
