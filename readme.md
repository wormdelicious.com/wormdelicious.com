# About
This is the source for the site [wormdelicious.com](https://wormdelicious.com). Worm Delicious is a firm that provides such products as Wormcoin™, Wormium™, Wormology™ items, and worm related wares in general.
# Contributing
Pull requests very much welcome. If you want to add new content, consider [creating an issue](https://codeberg.org/wormdelicious.com/wwwormdelicious/issues/new). Users with write permission are encouraged to consult Vee(CEO) before adding new content.
# Licensing
Content on this site is licensed under the "Be Gay, Do Crimes" license unless stated otherwise. Anything under this license is free to use & copy for any reason.
